# php-fpm

Custom php-fpm container image to run common php applications such as Wordpress and Drupal. 
Use together with nginx container by setting the `fastcgi_pass` in the nginx conf to `unix:/run/php7/fpm/www.sock`, eg:

    index index.php index.html index.htm;

    location ~* \.php$ {
        fastcgi_buffering off;
        #fastcgi_buffer_size 8k;
        fastcgi_read_timeout 1800;
        #fastcgi_split_path_info ^(.+\.php)(/.+)$;
        try_files $uri $uri/
        $document_root$fastcgi_script_name =404;
        fastcgi_intercept_errors on;
        fastcgi_index  index.php;
        include        fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $request_filename;
        fastcgi_pass   unix:/run/php7/fpm/www.sock;
    }

Make sure that the directory `/run/php7/fpm` is shared between the nginx and php-fpm containers using volume.

Available mount points:
* `/var/www` : Root directory of php applications
* `/var/lib/php7/session/www` : Session data storage
* `/var/lib/php7/wsdlcache/www` : Wsdlcache data storage
* `/run/php7/fpm` : Parent directory of the php-fpm unix socket
