FROM docker.io/library/alpine:3

ENV PHPFPM_SOCKET=/run/php7/fpm/www.sock
ENV PHPFPM_USER=nginx
ENV PHPFPM_GROUP=nginx
ENV PHPFPM_UID=101
ENV PHPFPM_GID=101
ENV HOME_DIR=/var/www/
ENV SMTP_HOST=smtp
ENV SMTP_PORT=25

RUN set -eux; \
    mkdir -p ${HOME_DIR} /run/php7/fpm /var/lib/php7/session/www /var/lib/php7/wsdlcache/www && \
    apk update && \
    apk add --no-cache \
        bash ca-certificates curl wget unzip gzip tar xz \
        git sed freetype ghostscript \
        php7-fpm php-cli php-soap composer \
        php7-pdo php7-pdo_mysql php7-pdo_sqlite php7-pdo_pgsql php7-mysqli php7-mysqlnd php7-pgsql \
        php7-exif php7-gd php7-iconv php7-pecl-gmagick php7-pecl-imagick \
        php7-opcache php7-pecl-mcrypt php7-session php7-ldap php7-bcmath php7-openssl \
        php7-fileinfo 
        php7-zip php7-zlib php7-curl php7-phar php7-json php7-mbstring php7-posix php7-xml php7-dom php7-simplexml 

WORKDIR ${HOME_DIR}
COPY docker-php-entrypoint /usr/local/bin/
COPY php.ini /etc/php7/templates/conf.d/
COPY www.conf /etc/php7/templates/php-fpm.d/

ENTRYPOINT ["docker-php-entrypoint"]
STOPSIGNAL SIGQUIT
CMD ["php-fpm7"]